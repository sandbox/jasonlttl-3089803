<?php

namespace Drupal\thycotic;

use \GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;

/**
 * Class Thycotic
 * Drupal integrated api for pulling data out of thycotic secret server.
 * @TODO: Can this be replaced by a more complete swagger generated client?
 * See https://swagger.io/tools/open-source/open-source-integrations/
 * @package Drupal\thycotic
 */
class Thycotic {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Oauth token which must be set via authenticate.
   * @var string
   */
  protected $token = NULL;

  /** @var string */
  protected $server;

  /** @var string */
  protected $username;

  /** @var string */
  protected $password;

  public function __construct(
    ClientInterface $client,
    string $server,
    string $username,
    string $password
  ) {
    $this->httpClient = $client;
    $this->server = $server;
    $this->username = $username;
    $this->password = $password;
  }

  /**
   * Returns true if the client is authenticated.
   * @return bool
   */
  public function authenticated() {
    return $this->token ? TRUE : FALSE;
  }

  /**
   * (Re)Authenticate our account.
   * @param bool $renew TRUE to renew the token.
   */
  public function authenticate($renew = FALSE) {
    if (!$this->authenticated() || $renew) {
      $this->token = $this->requestOauthToken($this->username, $this->password);
    }
  }

  /**
   * Returns an access token.
   * @param $username
   * @param $password
   * @return string
   */
  public function requestOauthToken($username, $password) {
    $token = null;

    /** @var Response */
    $response = $this->httpClient->post("{$this->server}/oauth2/token", [
      'form_params' => [
        'username' => $username,
        'password' => $password,
        'grant_type' => 'password'
      ]
    ]);

    // Test the response.
    if ($response->getStatusCode() == 200) {
      $token = json_decode($response->getBody())->access_token;
    }
    else {
      throw new Exception('Secret Server returned a non-200 response.');
    }

    return $token;
  }

  public function getSecret($secretId) {
    return $this->get("secrets/{$secretId}");
  }

  public function secretFieldOptions($secretId) {
    $fields = [];
    $secret = $this->getSecret($secretId);
    foreach ($secret->items as $item) {
      $fields[$item->fieldId] = $item->fieldName;
    }
    asort($fields);
    return $fields;
  }

  public function secretField($secretId, $fieldId) {
    $secret = $this->getSecret($secretId);
    foreach ($secret->items as $item) {
      if ($item->fieldId == $fieldId) {
        return $item->itemValue;
      }
    }
    return NULL;
  }

  /**
   * Returns a list of secrets suitable for a dropdown.
   * @param array $params - automatically adds required sortby params.
   * @return array
   */
  public function secretOptions(array $params=[]) {
    $secrets = [];
    $results = $this->searchSecrets($params);
    foreach ($results->records as $secret) {
      $secrets[$secret->id] = $secret->name;
    }
    return $secrets;
  }

  /**
   * Search secrets, assumes you have already authenticated.
   * @param array $params - automatically adds required sortby params.
   * @return mixed|null result or null.
   */
  public function searchSecrets(array $params=[]) {
    $defaults = [
      'sortBy[n].name' => 'name',
      'sortBy[n].direction' => 'Asc',
    ];
    return $this->get('secrets', array_merge($defaults, $params));
  }

  /**
   * Issues a get request with params as query string.
   * @param $resource /oauth2/token
   * @param $params query string params
   * @return mixed|null result or null.
   */
  public function get($resource, $params=[]) {
    $this->authenticate();

    $result = null;

    /**
     * @var Response;
     */
    $response = $this->httpClient->get($this->url($resource), [
      'headers' => [
        'Authorization' => "Bearer {$this->token}"
      ],
      'query' => $params
    ]);
    if ($response->getStatusCode() == 200) {
      $result = json_decode($response->getBody());
    }
    else {
      throw new Exception('Secret Server returned a non-200 response.');
    }
    return $result;
  }

  /**
   * @param $resource /oauth2/token
   * @param $params form params
   * @return mixed|null
   */
  public function post($resource, $params) {
    $this->authenticate();

    $results = null;
    $options = [
      'headers' => [
        'Authorization' => "Bearer {$this->token}"
      ],
      'form_params' => $params
    ];

    /** @var Response */
    $response = $this->httpClient->post($this->url($resource), $options);
    if ($response->getStatusCode() == 200) {
      $results = json_decode($response->getBody());
    }
    else {
      throw new Exception('Secret Server returned a non-200 response.');
    }

    return $results;
  }

  /**
   * Given a resource, returns fully qualified url.
   * @param $resource /oauth2/token
   * @return string https://nuclear.secrets/SecretServer/oauth2/token
   */
  protected function url($resource) {
    return "{$this->server}/api/v1/{$resource}";
  }
}
