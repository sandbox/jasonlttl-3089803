<?php

namespace Drupal\thycotic\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Google_Analytics settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'thycotic_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['thycotic.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('thycotic.settings');

    $form['intro'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Provide the username and password for a Thycotic service account.'),
    ];

    $form['server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Server'),
      '#default_value' => $config->get('server'),
      '#description' => $this->t('Address of your Secret Server API.<br>Example: https://nuclear.secrets/SecretServer'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('username'),
      '#description' => $this->t('Username for your service account.'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password (Insecure)'),
      '#description' => $this->t('You *should* store this using the key module or similar NOT here in configuration.'),
      '#default_value' => $config->get('password'),
      '#maxlength' => 255
    ];

    if ($config->get('password')) {
      drupal_set_message(t('You have configured a non-null password. You should probably not do that. Instead, use the key module.'), 'error');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('thycotic.settings');
    $config
      ->set('server', $form_state->getValue('server'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
