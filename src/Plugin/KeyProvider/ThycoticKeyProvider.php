<?php

/**
 * @file
 * Contains Drupal\thycotic\Plugin\KeyProvider\ThycoticKeyProvider.
 */

namespace Drupal\thycotic\Plugin\KeyProvider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\thycotic\Thycotic;
use Drupal\thycotic\ThycoticFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;

/**
 * Adds a key provider that allows a key to be stored in Thycotic.
 *
 * @KeyProvider(
 *   id = "thycotic",
 *   label = "Thycotic",
 *   description = @Translation("The Thycotic key provider stores the key in Thycotic Secret Server."),
 *   storage_method = "thycotic",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 */
class ThycoticKeyProvider extends KeyProviderBase implements KeyPluginFormInterface {

  /** @var ConfigFactoryInterface */
  protected $configFactory;

  /** @var ThycoticFactory */
  protected $thycoticFactory;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    ThycoticFactory $thycotic_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;
    $this->thycoticFactory = $thycotic_factory;

  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('thycotic.thycotic_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $config = $this->getConfiguration();
    $client = $this->thycoticFactory->createClient();

    $form['thycotic_secret_id'] = [
      '#title' => $this->t('Secret'),
      '#type' => 'select',
      '#options' => $client->secretOptions(),
      '#default_value' => $config['thycotic_secret_id'],
      '#description' => $this->t('Select the secret you would like to use.'),
    ];

    $form['thycotic_field_id'] = [
      '#title' => $this->t('Field'),
      '#type' => 'select',
      '#options' => $client->secretFieldOptions($config['thycotic_secret_id']),
      '#default_value' => $config['thycotic_field_id'],
      '#description' => $this->t('Select the field within the selected secret.')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    $config = $this->getConfiguration();
    $value = $this->uncache($config['thycotic_secret_id'], $config['thycotic_field_id']);
    if (!$value) {
      $value = $this->update($config['thycotic_secret_id'], $config['thycotic_field_id']);
    }
    return $value;
  }

  public function sync() {
    $config = $this->getConfiguration();

  }

  protected function uncache($secretId, $fieldId) {
    $cache = \Drupal::cache()->get($this->cid($secretId, $fieldId));
    return $cache ? $cache->data : null;
  }

  protected function update($secretId, $fieldId) {
    $client = $this->thycoticFactory->createClient();
    $value = $client->secretField($secretId, $fieldId);
    \Drupal::cache()->set(
      $this->cid($secretId, $fieldId),
      $value,
      time() + 24 * 3600
    );
    return $value;
  }

  protected function cid($secretId, $fieldId) {
    return 'thycotic:' . ((int) $secretId) . ':' . ((int) $fieldId);
  }

}
