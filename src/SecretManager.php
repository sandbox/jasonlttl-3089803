<?php

namespace Drupal\thycotic;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Drupal\thycotic\ThycoticFactory;
use Drupal\key\KeyRepository;

/**
 * Class SecretManager
 * @package Drupal\thycotic
 */
class SecretManager {

  /** @var \Drupal\key\KeyRepository */
  protected $keyRepository;

  /** @var \Drupal\thycotic\Thycotic */
  protected $thycotic;

  public function __construct(KeyRepository $keyRepository, ThycoticFactory $thycoticFactory) {
    $this->keyRepository = $keyRepository;
    $this->thycotic = $thycoticFactory->createClient();
  }

  /**
   * Enumerate all secrets.
   */
  public function all() {
    return $this->keyRepository->getKeysByProvider('thycotic');
  }

  /**
   * Sync all secrets.
   */
  public function sync() {
    $keys = $this->keyRepository->getKeysByProvider('thycotic');
    foreach ($keys as $id => $key) {
      $key->getKeyProvider()->sync();
    }
  }

  /**
   * Get a secret from the cache or live service if it's too old.
   * @param $id
   */
  public function get($id) {

  }

  /**
   * Cache a secret value.
   * @param $id
   * @param $value
   */
  public function cache($id, $value) {

  }
}
