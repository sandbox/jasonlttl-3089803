<?php

namespace Drupal\thycotic;

use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;

use Drupal\thycotic\Thycotic;

/**
 * Class ThycoticFactory
 * Drupal aware class to instantiate a Thycotic client based on configuration.
 * We separate this from the Thycotic client so it can be decoupled from Drupal
 * into an independent composer library at some point.
 * @package Drupal\thycotic
 */
class ThycoticFactory {
  /** @var ConfigFactory */
  protected $configFactory;

  /** @var ClientInterface */
  protected $httpClient;

  /**
   * ThycoticFactory constructor.
   * @param ConfigFactory $config
   * @param ClientInterface $client
   */
  public function __construct(ConfigFactory $config, ClientInterface $client) {
    $this->configFactory = $config;
    $this->httpClient = $client;
  }

  /**
   * Returns a new Thycotic client instantiated by Drupal.
   * @return \Drupal\thycotic\Thycotic
   */
  public function createClient() {
    static $thycotic;
    if (!$thycotic) {
      $config = $this->configFactory->get('thycotic.settings');
      $thycotic = new Thycotic($this->httpClient, $config->get('server'), $config->get('username'), $config->get('password'));
    }
    return $thycotic;
  }

}
