<?php

namespace Drupal\thycotic\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\thycotic\ThycoticFactory;
use Drupal\thycotic\Thycotic;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class ThycoticCommands extends DrushCommands {

  /** @var \Drupal\thycotic\Thycotic */
  protected $thycotic;

  public function __construct(ThycoticFactory $thycoticFactory) {
    $this->thycotic = $thycoticFactory->createClient();
  }

  /**
   * Fetches an Oauth token given credentials. Other commands use credentials configured in Drupal.
   *
   * @param string $username
   *   Username in Thycotic Secret Server.
   * @param string $password
   *   Password in Thycotic Secret Server.
   *
   * @command thycotic:authenticate
   * @aliases thy:auth
   * @usage thycotic:authenticate bob p@ssw0rd
   *   Your token is "12345". I have the same combination on my luggage!
   */
  public function authenticate($username, $password) {
    $token = $this->thycotic->requestOauthToken($username, $password);
    $this->output()->writeln("Your token is valid for 20 minutes:\n" . $token);
  }

  /**
   * Performs a search against the Secret Server api using Drupal's connection.
   *
   * @param string $search
   *   Search text for filter.searchText
   *
   * @command thycotic:search
   * @aliases thy:search
   * @usage thycotic:search nuclear
   */
  public function search($search=NULL) {
    $params = [];
    if ($search) {
      $params['filter.searchText'] = $search;
    }
    $secrets = $this->thycotic->secretOptions($params);
    $this->output()->writeln(print_r($secrets, true));
  }

  /**
   * Retrieves a secret from the API using Drupal's configuration.
   *
   * @param string $id
   *   Id of a Thycotic secret.
   *
   * @command thycotic:secret
   * @aliases thy:secret
   * @usage thycotic:secret 12345
   */
  public function secret($id) {
    $results = $this->thycotic->getSecret($id);
    $this->output()->writeln(print_r($results, true));
  }

  /**
   * Retrieves a secret from the API using Drupal's configuration.
   *
   * @param string $id
   *   Id of a Thycotic secret.
   *
   * @command thycotic:secret-fields
   * @aliases thy:secret-fields
   * @usage thycotic:secret-fields 12345
   */
  public function secretFields($id) {
    $results = $this->thycotic->secretFieldOptions($id);
    $this->output()->writeln(print_r($results, true));
  }
}
