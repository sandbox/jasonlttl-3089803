# Thycotic Key Provider

DISCLAIMER: This module was written by a random person on the internet
and is unaffiliated with the company [Thycotic]().

[Thycotic Secret Server](https://thycotic.com/products/secret-server/) is an
enterprise grade commercial product enabling secure management of secrets in cloud or
on premise. It is a cool system, allowing automated rotation, expiration policies,
lots of integrations, etc.

> Protect your privileged accounts with our enterprise-grade. Privileged Access Management (PAM) solution available
both on premise or in the cloud
